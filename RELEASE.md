## RELEASE NOTES [mcap-gw2-paymentservice-fd-india] ##

[1.4.0] 29 Nov 2018

* Updated TransactionState for ChargeBack,ChargeBackRefund original txn's.

[1.3.0] 16 Nov 2018

* Refund and chargeBackRefund api's request amount set to Transaction.refundAmount.
* Added merchantId,mcpBank and mcpAccout into Transaction.
* Added HealthController.

[1.2.0] 05 Nov 2018

* Update hm-fd-india version.

[1.1.6] 03 Oct 2018

* Set the base and mcp markup rates for all transactions.

[1.1.5] 05 Sep 2018

* Chargeback and chargeback refund service's added.

[1.1.4] 17 Aug 2018

* Truncated pan mask updated.

[1.1.3] 01 Aug 2018

* Modify mcp_txn.data and mcp_txn.original_oid column data storage.

[1.1.2] 27 Jul 2018

* Add toggle for logging request

[1.1.1] 26 Jul 2018

* Modify 3ds secure verification.

[1.1.0] 28 June 2018

* Converted to spring boot.
* Updated Secure configuration values taken from FdIndiaRoute.

[1.0.4] 05 June 2018

* Remove verification from acceptorTerminalId.

[1.0.3] 30 May 2018

* Modify the state update of the original transaction in PostAuth and Refund.
* Update gw2-common version.
* Add cut-off time check in VOID.

[1.0.2] 25 May 2018

* Modify storeId to use route.bank_mid.

[1.0.1] 23 May 2018

* Modify log4j2.xml file.
FROM mcpayment/alpine-java

ENV TZ=Asia/Singapore

ADD target/*.tar.gz /application
ADD docker-entrypoint.sh /

RUN set -xe; \
    adduser -D -u 2001 app.user; \
    chmod a+x /docker-entrypoint.sh
  
USER app.user

EXPOSE 10048

ENTRYPOINT [ "/docker-entrypoint.sh" ]

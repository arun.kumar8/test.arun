package com.mcpayment.paymentservice.error;

public class PaymentServiceException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 6113383132161897277L;
    private final String responseCode;

    public PaymentServiceException(Throwable cause) {
        super(cause);
        this.responseCode = null;
    }

    public PaymentServiceException(final String message) {
        super(message);
        this.responseCode = null;
    }

    public PaymentServiceException(final String responseCode, final String message) {
        super(message);
        this.responseCode = responseCode;
    }

    public PaymentServiceException(final String responseCode, final String message, final Throwable throwable) {
        super(message, throwable);
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }
}

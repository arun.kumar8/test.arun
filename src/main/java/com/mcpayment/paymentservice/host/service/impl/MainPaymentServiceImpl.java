package com.mcpayment.paymentservice.host.service.impl;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mcpayment.fd.data.entity.FdIndiaRoute;
import com.mcpayment.fd.data.service.FdIndiaRouteService;
import com.mcpayment.module.host.fd.constants.Constants;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.model.transaction.WalletMessage;
import com.mcpayment.module.service.CardPaymentModule;
import com.mcpayment.module.service.WalletPaymentModule;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.FdIndiaConstant;
import com.mcpayment.paymentservice.fd.exception.FdIndiaModuleException;
import com.mcpayment.paymentservice.fd.service.FdIndiaChargebackRefundService;
import com.mcpayment.paymentservice.fd.service.FdIndiaChargebackService;
import com.mcpayment.paymentservice.fd.service.FdIndiaInquiryService;
import com.mcpayment.paymentservice.fd.service.FdIndiaPostAuthService;
import com.mcpayment.paymentservice.fd.service.FdIndiaPreAuthService;
import com.mcpayment.paymentservice.fd.service.FdIndiaRefundService;
import com.mcpayment.paymentservice.fd.service.FdIndiaSaleService;
import com.mcpayment.paymentservice.fd.service.FdIndiaVoidService;
import com.mcpayment.paymentservice.host.service.MainPaymentService;

@Service
public class MainPaymentServiceImpl implements MainPaymentService {

    private static final Logger log = LoggerFactory.getLogger(MainPaymentServiceImpl.class);

    @Autowired
    private FdIndiaPostAuthService fdIndiaPostAuthService;

    @Autowired
    private FdIndiaPreAuthService fdIndiaPreAuthService;

    @Autowired
    private FdIndiaRefundService fdIndiaRefundService;

    @Autowired
    private FdIndiaSaleService fdIndiaSaleService;

    @Autowired
    private FdIndiaVoidService fdIndiaVoidService;

    @Autowired
    private FdIndiaInquiryService fdIndiaInquiryService;

    @Autowired
    private FdIndiaRouteService fdIndiaRouteService;

    @Autowired
    private FdIndiaChargebackService fdIndiaChargebackService;

    @Autowired
    private FdIndiaChargebackRefundService fdIndiaChargebackRefundService;

    @Value("${fd.keystore.path}")
    private String keystorePath;

    @Value("${fd.api.host.url}")
    private String apiHostUrl;

    @Value("${fd.ssl.protocol}")
    private String sslProtocol;

    @Value("${fd.isLogRequest}")
    private String isLogRequest;

    @Override
    public WalletMessage process(WalletMessage walletMessage) throws PaymentServiceException {
        WalletPaymentModule<WalletMessage> service = getService(walletMessage);
        log.info("Enter init.");
        walletMessage = (WalletMessage) service.init(walletMessage);
        log.info("Start save...   {}", walletMessage);
        walletMessage = (WalletMessage) service.save(walletMessage);
        log.info("Start send...");
        walletMessage = (WalletMessage) service.send(walletMessage);
        log.info("Start update... {}", walletMessage);
        walletMessage = (WalletMessage) service.update(walletMessage);
        return walletMessage;
    }

    @Override
    public TransactionMessage process(TransactionMessage transactionMessage) throws PaymentServiceException {
        log.debug("Charset : {}", Charset.defaultCharset().toString());
        transactionMessage = setAccountInfo(transactionMessage);
        CardPaymentModule<TransactionMessage> service = getService(transactionMessage);
        log.info("Enter init.");
        transactionMessage = (TransactionMessage) service.init(transactionMessage);
        log.info("Start prepare...");
        transactionMessage = (TransactionMessage) service.prepare(transactionMessage);
        log.info("Start save...   {}", transactionMessage);
        transactionMessage = (TransactionMessage) service.save(transactionMessage);
        log.info("Start send...");
        transactionMessage = (TransactionMessage) service.send(transactionMessage);
        log.info("Start update... {}", transactionMessage);
        transactionMessage = (TransactionMessage) service.update(transactionMessage);
        transactionMessage = removeAccountInfo(transactionMessage);
        return transactionMessage;
    }

    private TransactionMessage setAccountInfo(TransactionMessage message) {
        FdIndiaRoute route = fdIndiaRouteService.getFdIndiaRouteById(message.getServerData().getRouteOid());
        if (route == null) {
            throw new FdIndiaModuleException(
                    "Not Find FdIndiaRoute. routeOid : " + message.getServerData().getRouteOid());
        }
        message.setAcceptorId(route.getStoreId());
        message.setParticularData(Constants.KEYSTORE_PATH, keystorePath + route.getCertFileName());
        message.setParticularData(Constants.KEYSTORE_PASSWORD, route.getCertPassword());
        message.setParticularData(Constants.USER_NAME, route.getUserId());
        message.setParticularData(Constants.USER_PASSWORD, route.getPassword());
        message.setParticularData(Constants.API_HOST_URL, apiHostUrl);
        message.setParticularData(Constants.SSL_PROTOCOL, sslProtocol);
        message.setParticularData(Constants.IS_LOG_REQUEST, isLogRequest);
        message.setParticularData(FdIndiaConstant.BASE_RATE, route.getBaseRate());
        message.setParticularData(FdIndiaConstant.MCP_MARKUP_RATE, route.getMcpMarkUpRate());
        return message;
    }

    private TransactionMessage removeAccountInfo(TransactionMessage message) {
        message.setAcceptorId(null);
        message.setParticularData(Constants.KEYSTORE_PATH, null);
        message.setParticularData(Constants.KEYSTORE_PASSWORD, null);
        message.setParticularData(Constants.USER_NAME, null);
        message.setParticularData(Constants.USER_PASSWORD, null);
        message.setParticularData(Constants.API_HOST_URL, null);
        message.setParticularData(Constants.SSL_PROTOCOL, null);
        return message;
    }

    private CardPaymentModule<TransactionMessage> getService(TransactionMessage message) {
        CardPaymentModule<TransactionMessage> service = null;
        log.info("ps-FD-India----------message.getTransactionType()" + message.getTransactionType());
        switch (message.getTransactionType()) {
            case SALE:
                service = fdIndiaSaleService;
                break;
            case AUTHORIZATION:
                service = fdIndiaPreAuthService;
                break;
            case CAPTURE:
                service = fdIndiaPostAuthService;
                break;
            case REFUND:
                service = fdIndiaRefundService;
                break;
            case VOID:
                service = fdIndiaVoidService;
                break;
            case INQUIRY:
                service = fdIndiaInquiryService;
                break;
            case CHARGEBACK:
                service = fdIndiaChargebackService;
                break;
            case CHARGEBACK_REFUND:
                service = fdIndiaChargebackRefundService;
                break;
            default:
                log.error("no service defined for given transaction type < {} >.", message.getTransactionType());
                throw new PaymentServiceException("TRANSACTION TYPE NOT DEFINED");
        }
        return service;
    }

    private WalletPaymentModule<WalletMessage> getService(WalletMessage walletMessage) {
        WalletPaymentModule<WalletMessage> service = null;
        switch (walletMessage.getWalletPaymentType()) {
            case SUBMIT_QUICK_PAY:
                service = null;
                break;
            case SUBMIT_REFUND:
                break;
            case UNIFIED_ORDER:
                break;
            case CLOSE_ORDER:
                break;
            case QUERY_ORDER:
                break;
            case QUERY_REFUND:
                break;
            case TXN_HISTORY:
                break;
            case GET_CURRENCY_RATE:
                break;
            default:
                log.error("no service defined for given wallet payment type < {} >.",
                        walletMessage.getWalletPaymentType());
                throw new PaymentServiceException("WALLET PAYMENT TYPE NOT DEFINED");
        }
        return service;
    }

}
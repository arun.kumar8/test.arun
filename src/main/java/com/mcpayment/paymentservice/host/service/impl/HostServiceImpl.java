package com.mcpayment.paymentservice.host.service.impl;

import com.mcpayment.module.exception.ValidationException;
import com.mcpayment.module.model.Message;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.service.PaymentService;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.exception.FdIndiaModuleException;
import com.mcpayment.paymentservice.host.service.MainPaymentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("fdIndiaHostService")
public class HostServiceImpl implements PaymentService {

    private static final Logger log = LoggerFactory.getLogger(HostServiceImpl.class);

    @Autowired
    @Qualifier("mainPaymentServiceImpl")
    private MainPaymentService mainPaymentService;

    @Override
    public Message process(Message message) throws PaymentServiceException {
        TransactionMessage request = (TransactionMessage) message;
        try {
            return mainPaymentService.process(request);
        } catch (ValidationException | FdIndiaModuleException e) {
            log.warn("Transacton Error: {}", e);
            request.setErrorMessage(e.getMessage());
        } catch (Exception e) {
            log.error("Exception occured while processing in payment service", e);
            message.setErrorMessage(e.getMessage());
        }
        return request;
    }

    @Override
    public boolean keepAlive() {
        return true;
    }

}

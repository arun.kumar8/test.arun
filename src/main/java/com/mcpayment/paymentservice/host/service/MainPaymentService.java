package com.mcpayment.paymentservice.host.service;

import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.model.transaction.WalletMessage;

public interface MainPaymentService {
	WalletMessage process(WalletMessage walletMessage);
	TransactionMessage process(TransactionMessage transactionMessage);

}

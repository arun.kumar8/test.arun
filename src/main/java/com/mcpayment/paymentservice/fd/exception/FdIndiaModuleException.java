package com.mcpayment.paymentservice.fd.exception;

public class FdIndiaModuleException extends RuntimeException {

    private static final long serialVersionUID = -2738687823887852444L;

    private final String responseCode;

    public FdIndiaModuleException(final String message) {
        super(message);
        this.responseCode = null;
    }

    public FdIndiaModuleException(final String responseCode, final String message) {
        super(message);
        this.responseCode = responseCode;
    }

    public FdIndiaModuleException(final String responseCode, final String message, final Throwable throwable) {
        super(message, throwable);
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }
}

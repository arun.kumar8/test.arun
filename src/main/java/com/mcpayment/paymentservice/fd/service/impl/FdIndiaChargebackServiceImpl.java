package com.mcpayment.paymentservice.fd.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mcpayment.fd.data.entity.FdIndiaTransaction;
import com.mcpayment.fd.data.service.FdIndiaTransactionService;
import com.mcpayment.gateway.data.service.SequenceService;
import com.mcpayment.gateway.data.service.TransactionService;
import com.mcpayment.module.constant.GatewayConstant;
import com.mcpayment.module.exception.ValidationException;
import com.mcpayment.module.host.fd.constants.Constants;
import com.mcpayment.module.model.transaction.TransactionFields;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.model.transaction.TransactionState;
import com.mcpayment.module.model.transaction.TransactionType;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.FdIndiaConstant;
import com.mcpayment.paymentservice.fd.service.FdIndiaChargebackService;
import com.mcpayment.persistent.entity.Transaction;
import com.mcpayment.util.NumberUtil;
import com.mcpayment.util.ResponseCode;
import com.mcpayment.util.StringUtils;

@Service("fdIndiaChargebackService")
public class FdIndiaChargebackServiceImpl implements FdIndiaChargebackService {

    private static final Logger log = LoggerFactory.getLogger(FdIndiaChargebackServiceImpl.class);

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private FdIndiaTransactionService fdIndiaTransactionService;

    @Autowired
    private TransactionService transactionService;

    @Override
    public TransactionMessage init(TransactionMessage message) {
        log.info("Chargeback init method : {}", message);
        String originalTxn = StringUtils.getString(message.getServerData().getOriginalTransactionId());
        if (StringUtils.isBlank(originalTxn)) {
            message.setGatewayResponseCode(GatewayConstant.INVALID_ORG_TRANSACTION);
            throw new ValidationException(GatewayConstant.INVALID_ORG_TRANSACTION_STR);
        }
        if (StringUtils.isBlank(message.getTotalAmount())) {
            throw new ValidationException(GatewayConstant.INVALID_TOTAL_AMOUNT_ERROR);
        }
        if (StringUtils.isBlank(message.getCurrency())) {
            throw new ValidationException(GatewayConstant.INVALID_CURRENCY_ERROR);
        }
        return message;
    }

    @Override
    public TransactionMessage prepare(TransactionMessage message) {
        log.info("Chargeback prepare method : {}", message);
        Long currentStan = sequenceService.getNextStanSp(message.getAcceptorTerminalId());
        message.setStan(StringUtils.leftPad(String.valueOf(currentStan), 6, '0'));
        Long originalTransactionId = message.getServerData().getOriginalTransactionId();
        Transaction originalTransaction = transactionService.getTransactionById(originalTransactionId);
        log.info("[originalTransaction]= {}", originalTransaction);
        if (originalTransaction == null || (originalTransaction.getState().getId() != TransactionState.OK.getId()
                && originalTransaction.getState().getId() != TransactionState.CAPTURED.getId()
                && originalTransaction.getState().getId() != TransactionState.SETTLED.getId())) {
            message.setGatewayResponseCode(GatewayConstant.INVALID_ORG_TRANSACTION);
            throw new ValidationException(GatewayConstant.INVALID_ORG_TRANSACTION_STR);
        } else if (originalTransaction.getTotalAmount() != NumberUtil.getLong(message.getTotalAmount()).longValue()) {
            message.setGatewayResponseCode(GatewayConstant.OPERATION_DENIED);
            throw new ValidationException(GatewayConstant.AMOUNT_DOES_NOT_MATCH_ERROR);
        } else if (StringUtils.isBlank(message.getCurrency())
                || !originalTransaction.getCurrency().equals(message.getCurrency())) {
            message.setGatewayResponseCode(GatewayConstant.OPERATION_DENIED);
            throw new ValidationException(GatewayConstant.CURRECY_DOES_NOT_MATCH_ERROR);
        }
        message.setPan(originalTransaction.getPan());
        message.setTruncatedPan(originalTransaction.getTruncatedPan());
        message.setIccData(originalTransaction.getIccData());
        message.setBrandName(originalTransaction.getBrandName());
        message.setSalesAmount(StringUtils.getString(originalTransaction.getSaleAmount()));
        message.setServiceAmount(StringUtils.getString(originalTransaction.getServiceAmount()));
        message.setServiceRate(StringUtils.getString(originalTransaction.getServiceRate()));
        message.setGstAmount(StringUtils.getString(originalTransaction.getGstAmount()));
        message.setGstRate(StringUtils.getString(originalTransaction.getGstRate()));
        message.setTotalAmount(StringUtils.getString(originalTransaction.getTotalAmount()));
        message.setApplnPanSeq(originalTransaction.getApplnPanSeq());
        message.setHostResponseDate(originalTransaction.getHostResponseDate());
        message.setRrn(originalTransaction.getRrn());
        message.setAuthCode(originalTransaction.getAuthorizationCode());
        message.setHostResponseCode(originalTransaction.getHostResponseCode());
        message.setReceiptNumber(originalTransaction.getReceiptNo());
        message.setParticularData(TransactionFields.ORIGINAL_TRANSACTION_ID, originalTransaction.getId());
        message.setParticularData(TransactionFields.ORIGINAL_RECEIPT_NO, originalTransaction.getReceiptNo());
        message.setParticularData(TransactionFields.ORIGINAL_STAN, originalTransaction.getStan());
        message.setParticularData(TransactionFields.ORIGINAL_HOST_RESPONSE_TIMESTAMP,
                originalTransaction.getHostResponseDate());
        message.setParticularData(TransactionFields.ORIGINAL_CLIENT_REQUEST_TIMESTAMP,
                originalTransaction.getClientRequestDate());
        message.setParticularData(TransactionFields.ORIGINAL_GATEWAY_REQUEST_TIMESTAMP,
                originalTransaction.getGatewayRequestDate());
        message.setParticularData(TransactionFields.ORIGINAL_GATEWAY_RESPONSE_TIMESTAMP,
                originalTransaction.getGatewayResponseDate());
        return message;
    }

    @Override
    public TransactionMessage save(TransactionMessage message) {
        log.info("Chargeback save method : {}", message);
        Transaction transaction = new Transaction();
        transaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        transaction.setState(TransactionState.REQUESTED);
        message.setTransactionState(StringUtils.getString(transaction.getState().getId()));
        transaction.setSaleAmount(NumberUtil.getLong(message.getSalesAmount()));
        transaction.setServiceAmount(NumberUtil.getLong(message.getServiceAmount()));
        transaction.setGstAmount(NumberUtil.getLong(message.getGstAmount()));
        transaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        transaction.setServiceRate(NumberUtil.getLong(message.getServiceRate()));
        transaction.setGstRate(NumberUtil.getLong(message.getGstRate()));
        transaction.setStan(message.getStan());
        transaction.setGatewayRequestDate(new Date());
        transaction.setClientRequestDate(message.getClientRequestDate());
        transaction.setNewRouteId(message.getServerData().getRouteOid());
        transaction.setReferenceNo(message.getParticularDataAsString(GatewayConstant.REFERENCE));
        transaction.setAcceptorId(message.getAcceptorId());
        transaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
        transaction.setAcceptorIPPTerminalId(message.getServerData().getRoute().getAcceptorIPPTerminalId());
        transaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        transaction.setPan(message.getPan());
        transaction.setIccData(message.getIccData());
        transaction.setCardExpiryDate(message.getCardExpiryDate());
        transaction.setCardEntryMode(message.getCardEntryMode());
        transaction.setApplnPanSeq(message.getApplnPanSeq());
        transaction.setInstallment(message.getInstallment());
        transaction.setTruncatedPan(message.getTruncatedPan());
        transaction.setOriginalOid(message.getServerData().getOriginalTransactionId());
        transaction.setRemarks(message.getParticularDataAsString(GatewayConstant.REMARKS));
        transaction.setBrandName(message.getBrandName());
        transaction.setCardHolderName(message.getCardHolderName());
        transaction.setCurrency(message.getCurrency());
        transaction.setPosConditionCode(NumberUtil.getValidateInteger(message.getPosConditionCode()));
        transaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        transaction.setMcpMarkupRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        for (String key : message.getParticularDataKeys()) {
            transaction.getDataAsJsonObject().addProperty(key, String.valueOf(message.getParticularData(key)));
        }
        transaction.storeJsonObjectOnData();
        transaction.setMerchantId(message.getServerData().getMerchantOid());
        transaction.setMcpBank(message.getServerData().getRoute().getMcpBank());
        transaction.setMcpAccount(message.getServerData().getRoute().getMcpAccount());
        Long txnId = transactionService.insertTransaction(transaction);
        message.setTransactionId(String.valueOf(txnId));

        FdIndiaTransaction fdIndiaTransaction = new FdIndiaTransaction();
        fdIndiaTransaction.setId(txnId);
        fdIndiaTransaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        fdIndiaTransaction
                .setState(TransactionState.getTransactionState(NumberUtil.getInteger(message.getTransactionState())));
        fdIndiaTransaction.setMchId(message.getAcceptorId());
        fdIndiaTransaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        fdIndiaTransaction.setTransactionId(message.getTransactionId());
        fdIndiaTransaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        fdIndiaTransaction.setItemsDetails(message.getReferenceText());
        fdIndiaTransaction.setCurrency(message.getCurrency());
        fdIndiaTransaction.setTimeStart(new Date());
        fdIndiaTransaction.setOriginTxnId(StringUtils.getString(message.getServerData().getOriginalTransactionId()));
        fdIndiaTransaction.setBrandName(message.getBrandName());
        fdIndiaTransaction.setCvc(message.getCvc());
        fdIndiaTransaction.setCardHolderName(message.getCardHolderName());
        fdIndiaTransaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        fdIndiaTransaction.setMcpMarkUpRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        transaction.setMerchantId(message.getServerData().getMerchantOid());
        transaction.setMcpBank(message.getServerData().getRoute().getMcpBank());
        transaction.setMcpAccount(message.getServerData().getRoute().getMcpAccount());
        fdIndiaTransactionService.insertTransaction(fdIndiaTransaction);
        return message;
    }

    @Override
    public TransactionMessage send(TransactionMessage message) {
        message.setHostResponseCode(ResponseCode.SUCCESS);
        message.setHostResponseDate(new Date());
        message.setGatewayResponseDate(message.getHostResponseDate());
        return message;
    }

    @Override
    public TransactionMessage update(TransactionMessage message) {
        try {
            log.info("Chargeback update method : {}", message);
            // Update DB once response got from HM
            Long txnId = Long.parseLong(message.getTransactionId());
            Transaction transaction = transactionService.getTransactionById(txnId);
            log.info("Chargeback update method(transaction) : {}", transaction);
            if (transaction != null) {
                transaction.setHostResponseDate(message.getHostResponseDate());
                transaction.setGatewayResponseDate(message.getGatewayResponseDate());
                transaction.setAuthorizationDate(new Date());
                transaction.setAuthorizationCode(message.getAuthCode());
                FdIndiaTransaction fdIndiaTransaction = fdIndiaTransactionService.getTransactionById(txnId);
                log.info("Chargeback update method(fdIndiaTransaction) : {}", fdIndiaTransaction);
                if (ResponseCode.SUCCESS.equals(message.getHostResponseCode())) {
                    message.setGatewayResponseCode(ResponseCode.SUCCESS);
                    transaction.setState(TransactionState.OK);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setRrn(message.getRrn());
                    transaction.setReceiptNo(message.getReceiptNumber());
                    Long originalTransactionId = message.getServerData().getOriginalTransactionId();
                    Transaction original = transactionService.getTransactionById(originalTransactionId);
                    original.setState(TransactionState.RETRIEVAL);
                    transactionService.updateTransaction(original);
                    FdIndiaTransaction originalFdTxn = fdIndiaTransactionService.getTransactionById(original.getId());
                    originalFdTxn.setState(TransactionState.REFUNDED);
                    fdIndiaTransactionService.updateTransaction(originalFdTxn);
                    fdIndiaTransaction.setState(TransactionState.OK);
                    fdIndiaTransaction.setReceiptNo(message.getReceiptNumber());
                } else {
                    message.setGatewayResponseCode(ResponseCode.REQUEST_DENIED);
                    transaction.setState(TransactionState.DENIED);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setErrorMessage(message.getHostResponseMessage() + " : " + message.getErrorMessage());
                    fdIndiaTransaction.setState(TransactionState.DENIED);
                    fdIndiaTransaction.setErrMsg(message.getErrorMessage());
                }
                transactionService.updateTransaction(transaction);
                fdIndiaTransaction.setCountryName(message.getParticularDataAsString(Constants.COUNTRY));
                fdIndiaTransaction.setPaymentType(message.getParticularDataAsString(Constants.PAYMENT_TYPE));
                fdIndiaTransaction.setResponseMsg(message.getHostResponseMessage());
                fdIndiaTransaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
                fdIndiaTransaction.setHostTxnTime(message.getHostResponseDate());
                fdIndiaTransaction.setMcpTxnTime(new Date());
                fdIndiaTransaction.setTimeEnd(new Date());
                fdIndiaTransactionService.updateTransaction(fdIndiaTransaction);
            } else {
                throw new PaymentServiceException(GatewayConstant.INVALID_TRANSACTION_STR);
            }
        } catch (Exception e) {
            log.error("error {}", e);
        }
        return message;
    }

}

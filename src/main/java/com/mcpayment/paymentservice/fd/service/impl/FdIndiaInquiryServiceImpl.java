package com.mcpayment.paymentservice.fd.service.impl;

import com.mcpayment.module.host.fd.services.FdIndiaHostModuleService;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.exception.FdIndiaModuleException;
import com.mcpayment.paymentservice.fd.service.FdIndiaInquiryService;
import com.mcpayment.util.JsonUtil;
import com.mcpayment.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("fdIndiaInquiryService")
public class FdIndiaInquiryServiceImpl implements FdIndiaInquiryService {

    private static final Logger log = LoggerFactory.getLogger(FdIndiaInquiryServiceImpl.class);

    @Autowired
    private FdIndiaHostModuleService fdIndiaHostModuleService;

    @Override
    public TransactionMessage init(TransactionMessage message) {
        log.info("Inquiry init method : {}", message);
        // TODO Auto-generated method stub
        String transactionId = message.getTransactionId();
        if (StringUtils.isBlank(transactionId)) {
            throw new FdIndiaModuleException("Invalid TransactionId.");
        }

        return message;
    }

    @Override
    public TransactionMessage prepare(TransactionMessage message) {
        log.info("Inquiry prepare method : {}", message);
        // TODO Auto-generated method stub
        return message;
    }

    @Override
    public TransactionMessage save(TransactionMessage message) {
        log.info("Inquiry save method : {}", message);
        //TODO Set common transaction data and insert to MCP_TRANSACTION
        return message;
    }

    @Override
    public TransactionMessage send(TransactionMessage message) {
        log.info("Inquiry send method : {}", message);
        try {
            List<TransactionMessage> messageList = fdIndiaHostModuleService.inquiry(message);
            message.setDecJsonData(JsonUtil.getGson().toJson(messageList));
        } catch (Exception e) {
            throw new PaymentServiceException(e);
        }
        return message;
    }

    @Override
    public TransactionMessage update(TransactionMessage message) {
        log.info("Inquiry update method : {}", message);
        return message;
    }
}

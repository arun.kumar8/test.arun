package com.mcpayment.paymentservice.fd.service.impl;

import com.mcpayment.gateway.data.service.MpiTransactionService;
import com.mcpayment.module.constant.ThreedSecureConst;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.FdIndiaConstant;
import com.mcpayment.paymentservice.fd.service.FdIndiaSecureService;
import com.mcpayment.persistent.entity.MpiTransaction;
import com.mcpayment.util.Const;
import com.mcpayment.util.NumberUtil;
import com.mcpayment.util.ResponseCode;
import com.mcpayment.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Luo Ting on 2018/7/26
 */
@Service("fdIndiaSecureService")
public class FdIndiaSecureServiceImpl implements FdIndiaSecureService {

    private static final Logger log = LoggerFactory.getLogger(FdIndiaSecureServiceImpl.class);

    @Autowired
    private MpiTransactionService mpiTransactionService;

    @Override
    public TransactionMessage validateAndSet3DSecureInfo(TransactionMessage message) {
        String transactionReference = message.getParticularDataAsString(ThreedSecureConst.TRANSACTION_REFERENCE);
        String threedSecure = message.getParticularDataAsString(ThreedSecureConst.THREEDSECURE);
        String eci = message.getParticularDataAsString(ThreedSecureConst.ECI);
        String xid = message.getParticularDataAsString(ThreedSecureConst.XID);
        String cav = message.getParticularDataAsString(ThreedSecureConst.CAV);

        if (StringUtils.isBlank(eci) && StringUtils.isBlank(xid) && StringUtils.isBlank(cav)) {
            if (!StringUtils.isBlank(transactionReference)) {
                List<MpiTransaction> mpiTransactions = null;
                try {
                    mpiTransactions = mpiTransactionService.getMpiTransactionByParams(message.getServerData()
                                    .getMcpTerminalId(), transactionReference, NumberUtil.getLong(message.getTotalAmount()),
                            message.getCurrency(), null, null);
                } catch (Exception e) {
                    log.error("Unable get MPI_TRANSACTION data with reference : {} due : {}", transactionReference, e);
                    mpiTransactions = mpiTransactionService.getMpiTransactionByParams(message.getServerData()
                                    .getMcpTerminalId(), transactionReference, NumberUtil.getLong(message.getTotalAmount()),
                            message.getCurrency(), null, null);
                }
                MpiTransaction mpiTransaction = null;
                if (mpiTransactions != null && mpiTransactions.size() > 0) {
                    if (mpiTransactions.size() > 1) {
                        log.error("Multiple mpi transactions has with same reference : {}", transactionReference);
                    } else {
                        mpiTransaction = mpiTransactions.get(0);
                    }
                }
                if (mpiTransaction != null) {
                    log.info("mpiTransaction.getId: " + mpiTransaction.getId() + " and mpiTransaction.getReference: "
                            + mpiTransaction.getReference());
                    if (mpiTransaction.getEci() != null) {
                        eci = StringUtils.getString(mpiTransaction.getEci());
                        message.setParticularData(ThreedSecureConst.ECI, eci);

                        threedSecure = mpiTransaction.getEnrollementStatus();
                        message.setParticularData(ThreedSecureConst.THREEDSECURE, threedSecure);
                    }
                    if (!StringUtils.isBlank(mpiTransaction.getEncryptedXID())) {
                        xid = StringUtils.getString(mpiTransaction.getEncryptedXID());
                        message.setParticularData(ThreedSecureConst.XID, xid);
                    }
                    if (!StringUtils.isBlank(mpiTransaction.getEncryptedCAVV())) {
                        cav = StringUtils.getString(mpiTransaction.getEncryptedCAVV());
                        message.setParticularData(ThreedSecureConst.CAV, cav);
                    }
                }
            }
        }
        log.info("After 3DSecure Validation >> Reference : " + transactionReference + " threedSecure: " + threedSecure
                + " eci: " + eci + " xid: " + StringUtils.maskPAN(xid) + " cav: " + StringUtils.maskPAN(cav));

        if (!StringUtils.isBlank(threedSecure) && threedSecure.equals(Const.MERCHANT_HAS_3DS_ENROLLED_YES)) {
            if (StringUtils.isBlank(eci)) {
                message.setParticularData(ThreedSecureConst.THREEDSECURE, "0");
                log.warn(FdIndiaConstant.INVALID_ECI_STR);
                throw new PaymentServiceException(ResponseCode.PARAMETERS_INVALID, FdIndiaConstant.INVALID_ECI_STR);
            } else if (!isECIApproved(Integer.parseInt(eci), message.getServerData().getRoute().getBrand().getName())) {
                message.setParticularData(ThreedSecureConst.THREEDSECURE, "0");
                log.warn(FdIndiaConstant.INVALID_ECI_STR);
                throw new PaymentServiceException(ResponseCode.PARAMETERS_INVALID, FdIndiaConstant.INVALID_ECI_STR);
            }
            if (StringUtils.isBlank(xid)) {
                message.setParticularData(ThreedSecureConst.THREEDSECURE, "0");
                log.warn(FdIndiaConstant.INVALID_XID_STR);
                throw new PaymentServiceException(ResponseCode.PARAMETERS_INVALID, FdIndiaConstant.INVALID_XID_STR);
            }
            if (StringUtils.isBlank(cav)) {
                message.setParticularData(ThreedSecureConst.THREEDSECURE, "0");
                log.warn(FdIndiaConstant.INVALID_CAV_STR);
                throw new PaymentServiceException(ResponseCode.PARAMETERS_INVALID, FdIndiaConstant.INVALID_CAV_STR);
            }
        }

        return message;
    }

    private boolean isECIApproved(Integer eci, String brandName) {
        log.info("ECI : {} BrandName : {} ", eci, brandName);
        if (!brandName.equalsIgnoreCase(Const.BRAND_NAME_VISA) && !brandName.equalsIgnoreCase(Const.BRAND_NAME_MASTER)
                && !brandName.equalsIgnoreCase(Const.BRAND_NAME_JCB) && brandName.equalsIgnoreCase(Const.BRAND_NAME_AMEX)) {
            return true;
        } else if (!brandName.equalsIgnoreCase(Const.BRAND_NAME_VISA) || (eci != 5 && eci != 6)) {
            if (brandName.equalsIgnoreCase(Const.BRAND_NAME_MASTER) && (eci == 1 || eci == 2)) {
                return true;
            } else if (brandName.equalsIgnoreCase(Const.BRAND_NAME_JCB) && eci == 5) {
                return true;
            } else {
                return brandName.equalsIgnoreCase(Const.BRAND_NAME_AMEX) && (eci == 5 || eci == 6);
            }
        } else {
            return true;
        }
    }
}

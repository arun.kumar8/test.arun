package com.mcpayment.paymentservice.fd.service.impl;

import com.mcpayment.fd.data.entity.FdIndiaTransaction;
import com.mcpayment.fd.data.service.FdIndiaTransactionService;
import com.mcpayment.gateway.data.service.CurrencyService;
import com.mcpayment.gateway.data.service.SequenceService;
import com.mcpayment.gateway.data.service.TransactionService;
import com.mcpayment.module.constant.GatewayConstant;
import com.mcpayment.module.crypto.util.DataSecurity;
import com.mcpayment.module.host.fd.constants.Constants;
import com.mcpayment.module.host.fd.services.FdIndiaHostModuleService;
import com.mcpayment.module.model.transaction.TransactionFields;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.model.transaction.TransactionState;
import com.mcpayment.module.model.transaction.TransactionType;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.FdIndiaConstant;
import com.mcpayment.paymentservice.fd.exception.FdIndiaModuleException;
import com.mcpayment.paymentservice.fd.service.FdIndiaVoidService;
import com.mcpayment.persistent.entity.Transaction;
import com.mcpayment.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("fdIndiaVoidService")
public class FdIndiaVoidServiceImpl implements FdIndiaVoidService {

    private static final Logger log = LoggerFactory.getLogger(FdIndiaVoidServiceImpl.class);

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private FdIndiaTransactionService fdIndiaTransactionService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private FdIndiaHostModuleService fdIndiaHostModuleService;

    @Override
    public TransactionMessage init(TransactionMessage message) {
        log.info("Void init method : {}", message);
        String originalTxn = StringUtils.getString(message.getServerData().getOriginalTransactionId());
        if (StringUtils.isBlank(originalTxn)) {
            throw new FdIndiaModuleException("Invalid OriginalTransactionId.");
        }
        String currency = message.getCurrency();
        if (StringUtils.isBlank(currency)) {
            throw new FdIndiaModuleException("Invalid Currency.");
        }

        return message;
    }

    @Override
    public TransactionMessage prepare(TransactionMessage message) {
        log.info("Void prepare method : {}", message);
        Long currentStan = sequenceService.getNextStanSp(message.getAcceptorTerminalId());
        message.setStan(StringUtils.leftPad(String.valueOf(currentStan), 6, '0'));

        Long originalTransactionId = message.getServerData().getOriginalTransactionId();
        Transaction originalTransaction = transactionService.getTransactionById(originalTransactionId);
        log.info("[originalTransaction]= {}", originalTransaction);
        if (originalTransaction == null
                || (originalTransaction != null && originalTransaction.getState().getId() != TransactionState.OK.getId()
                        && originalTransaction.getState().getId() != TransactionState.CAPTURED.getId())) {
            message.setGatewayResponseCode(GatewayConstant.INVALID_ORG_TRANSACTION);
            throw new FdIndiaModuleException(GatewayConstant.INVALID_ORG_TRANSACTION_STR);
        }
        if (DateUtil.isCutOffTimeExceeded(originalTransaction.getClientRequestDate(),
                message.getParticularDataAsString(GatewayConstant.CUTOFF_TIME),
                message.getParticularDataAsString(GatewayConstant.DEFAULT_CUTOFF_TIME), TransactionType.VOID)) {
            message.setGatewayResponseCode(GatewayConstant.OPERATION_DENIED);
            throw new FdIndiaModuleException(GatewayConstant.SETTLED_ALREADY_ERROR);
        }

        message.setPan(originalTransaction.getPan());
        message.setTruncatedPan(originalTransaction.getTruncatedPan());
        message.setIccData(originalTransaction.getIccData());
        message.setProcessingCode("020000");
        message.setBrandName(originalTransaction.getBrandName());
        message.setSalesAmount(StringUtils.getString(originalTransaction.getSaleAmount()));
        message.setServiceAmount(StringUtils.getString(originalTransaction.getServiceAmount()));
        message.setServiceRate(StringUtils.getString(originalTransaction.getServiceRate()));
        message.setGstAmount(StringUtils.getString(originalTransaction.getGstAmount()));
        message.setGstRate(StringUtils.getString(originalTransaction.getGstRate()));
        message.setTotalAmount(StringUtils.getString(originalTransaction.getTotalAmount()));
        message.setApplnPanSeq(originalTransaction.getApplnPanSeq());
        message.setHostResponseDate(originalTransaction.getHostResponseDate());
        message.setRrn(originalTransaction.getRrn());
        message.setAuthCode(originalTransaction.getAuthorizationCode());
        message.setHostResponseCode(originalTransaction.getHostResponseCode());
        message.setReceiptNumber(originalTransaction.getReceiptNo());
        message.setParticularData(TransactionFields.ORIGINAL_TRANSACTION_ID, originalTransaction.getId());
        message.setParticularData(TransactionFields.ORIGINAL_RECEIPT_NO, originalTransaction.getReceiptNo());
        message.setParticularData(TransactionFields.ORIGINAL_STAN, originalTransaction.getStan());
        message.setParticularData(TransactionFields.ORIGINAL_HOST_RESPONSE_TIMESTAMP,
                originalTransaction.getHostResponseDate());
        message.setParticularData(TransactionFields.ORIGINAL_CLIENT_REQUEST_TIMESTAMP,
                originalTransaction.getClientRequestDate());
        message.setParticularData(TransactionFields.ORIGINAL_GATEWAY_REQUEST_TIMESTAMP,
                originalTransaction.getGatewayRequestDate());
        message.setParticularData(TransactionFields.ORIGINAL_GATEWAY_RESPONSE_TIMESTAMP,
                originalTransaction.getGatewayResponseDate());

        return message;
    }

    @Override
    public TransactionMessage save(TransactionMessage message) {
        log.info("Void save method : {}", message);
        Transaction transaction = new Transaction();
        transaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        transaction.setState(TransactionState.REQUESTED);
        message.setTransactionState(StringUtils.getString(transaction.getState().getId()));
        transaction.setSaleAmount(NumberUtil.getLong(message.getSalesAmount()));
        transaction.setServiceAmount(NumberUtil.getLong(message.getServiceAmount()));
        transaction.setGstAmount(NumberUtil.getLong(message.getGstAmount()));
        transaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        transaction.setServiceRate(NumberUtil.getLong(message.getServiceRate()));
        transaction.setGstRate(NumberUtil.getLong(message.getGstRate()));
        transaction.setStan(message.getStan());
        transaction.setGatewayRequestDate(new Date());
        transaction.setClientRequestDate(message.getClientRequestDate());
        transaction.setNewRouteId(message.getServerData().getRouteOid());
        transaction.setRemarks(message.getReferenceText());
        transaction.setAcceptorId(message.getAcceptorId());
        transaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
        transaction.setAcceptorIPPTerminalId(message.getServerData().getRoute().getAcceptorIPPTerminalId());
        transaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        transaction.setPan(message.getPan());
        transaction.setIccData(message.getIccData());
        transaction.setCardExpiryDate(message.getCardExpiryDate());
        transaction.setCardEntryMode(message.getCardEntryMode());
        transaction.setApplnPanSeq(message.getApplnPanSeq());
        transaction.setInstallment(message.getInstallment());
        transaction.setTruncatedPan(message.getTruncatedPan());
        transaction.setBrandName(message.getBrandName());
        transaction.setCardHolderName(message.getCardHolderName());
        transaction.setCurrency(message.getCurrency());
        transaction.setPosConditionCode(NumberUtil.getValidateInteger(message.getPosConditionCode()));
        transaction.setOriginalOid(message.getServerData().getOriginalTransactionId());
        transaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        transaction.setMcpMarkupRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        transaction.setMerchantId(message.getServerData().getMerchantOid());
        transaction.setMcpBank(message.getServerData().getRoute().getMcpBank());
        transaction.setMcpAccount(message.getServerData().getRoute().getMcpAccount());
        Long txnId = transactionService.insertTransaction(transaction);
        message.setTransactionId(String.valueOf(txnId));

        FdIndiaTransaction fdIndiaTransaction = new FdIndiaTransaction();
        fdIndiaTransaction.setId(txnId);
        fdIndiaTransaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        fdIndiaTransaction
                .setState(TransactionState.getTransactionState(NumberUtil.getInteger(message.getTransactionState())));
        fdIndiaTransaction.setMchId(message.getAcceptorId());
        fdIndiaTransaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        fdIndiaTransaction.setTransactionId(message.getTransactionId());
        fdIndiaTransaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        fdIndiaTransaction.setItemsDetails(message.getReferenceText());
        fdIndiaTransaction.setCurrency(message.getCurrency());
        fdIndiaTransaction.setTimeStart(new Date());
        fdIndiaTransaction.setOriginTxnId(StringUtils.getString(message.getServerData().getOriginalTransactionId()));
        fdIndiaTransaction.setBrandName(message.getBrandName());
        fdIndiaTransaction.setCvc(message.getCvc());
        fdIndiaTransaction.setCardHolderName(message.getCardHolderName());
        fdIndiaTransaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        fdIndiaTransaction.setMcpMarkUpRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        fdIndiaTransactionService.insertTransaction(fdIndiaTransaction);

        return message;
    }

    @Override
    public TransactionMessage send(TransactionMessage message) {
        // Send message to HM to process request to acquirer host
        try {
            log.info("Void send method : {}", message);
            message = fdIndiaHostModuleService.purchaseVoid(message);
        } catch (Exception e) {
            throw new PaymentServiceException(e);
        }
        return message;
    }

    @Override
    public TransactionMessage update(TransactionMessage message) {
        log.info("Void update method : {}", message);
        try {
            // Update DB once response got from HM
            Long txnId = Long.parseLong(message.getTransactionId());
            Transaction transaction = transactionService.getTransactionById(txnId);
            log.info("Void update method(transaction) : {}", transaction);
            if (transaction != null) {
                transaction.setHostResponseDate(message.getHostResponseDate());
                transaction.setGatewayResponseDate(message.getGatewayResponseDate());
                transaction.setAuthorizationDate(new Date());
                transaction.setAuthorizationCode(message.getAuthCode());

                // Set specific transaction data and insert to
                // FDINDIA_TRANSACTION, This process should asynchronized task
                FdIndiaTransaction fdIndiaTransaction = fdIndiaTransactionService.getTransactionById(txnId);
                log.info("Void update method(fdIndiaTransaction) : {}", fdIndiaTransaction);
                if (ResponseCode.SUCCESS.equals(message.getHostResponseCode())) {
                    message.setHostResponseCode(ResponseCode.SUCCESS);
                    message.setGatewayResponseCode(ResponseCode.SUCCESS);

                    transaction.setState(TransactionState.OK);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setRrn(message.getRrn());
                    transaction.setReceiptNo(message.getReceiptNumber());

                    Long originalTransactionId = message.getServerData().getOriginalTransactionId();
                    Transaction original = transactionService.getTransactionById(originalTransactionId);
                    if (original != null && (original.getState().getId() == TransactionState.OK.getId()
                            || original.getState().getId() == TransactionState.CAPTURED.getId())) {
                        original.setState(TransactionState.VOIDED);
                        transactionService.updateTransaction(original);
                    }

                    fdIndiaTransaction.setState(TransactionState.OK);
                    fdIndiaTransaction.setReceiptNo(message.getReceiptNumber());

                    FdIndiaTransaction originalFdTxn = fdIndiaTransactionService.getTransactionById(original.getId());
                    if (originalFdTxn != null && (originalFdTxn.getState().getId() == TransactionState.OK.getId()
                            || originalFdTxn.getState().getId() == TransactionState.CAPTURED.getId())) {
                        originalFdTxn.setState(TransactionState.VOIDED);
                        fdIndiaTransactionService.updateTransaction(originalFdTxn);
                    }
                } else {
                    // message.setHostResponseCode(ResponseCode.REQUEST_DENIED);
                    message.setGatewayResponseCode(ResponseCode.REQUEST_DENIED);

                    transaction.setState(TransactionState.DENIED);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setErrorMessage(message.getHostResponseMessage() + " : " + message.getErrorMessage());

                    fdIndiaTransaction.setState(TransactionState.DENIED);
                    fdIndiaTransaction.setErrMsg(message.getErrorMessage());
                }
                transactionService.updateTransaction(transaction);

                fdIndiaTransaction.setCountryName(message.getParticularDataAsString(Constants.COUNTRY));
                fdIndiaTransaction.setPaymentType(message.getParticularDataAsString(Constants.PAYMENT_TYPE));
                fdIndiaTransaction.setResponseMsg(message.getHostResponseMessage());
                fdIndiaTransaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
                fdIndiaTransaction.setHostTxnTime(message.getHostResponseDate());
                fdIndiaTransaction.setMcpTxnTime(new Date());
                fdIndiaTransaction.setTimeEnd(new Date());
                fdIndiaTransactionService.updateTransaction(fdIndiaTransaction);
            } else {
                throw new PaymentServiceException("Invalid Transaction.");
            }
        } catch (Exception e) {
            log.error("error {}", e);
        }
        return message;
    }

}
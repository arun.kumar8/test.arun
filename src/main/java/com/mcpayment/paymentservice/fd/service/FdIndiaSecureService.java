package com.mcpayment.paymentservice.fd.service;

import com.mcpayment.module.model.transaction.TransactionMessage;

/**
 * Created by Luo Ting on 2018/7/26
 */
public interface FdIndiaSecureService {

    TransactionMessage validateAndSet3DSecureInfo(TransactionMessage message);
}

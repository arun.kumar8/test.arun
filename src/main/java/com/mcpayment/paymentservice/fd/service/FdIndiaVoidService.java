package com.mcpayment.paymentservice.fd.service;

import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.service.CardPaymentModule;

public interface FdIndiaVoidService extends CardPaymentModule<TransactionMessage> {
}

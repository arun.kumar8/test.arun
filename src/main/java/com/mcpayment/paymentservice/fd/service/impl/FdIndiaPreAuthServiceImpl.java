package com.mcpayment.paymentservice.fd.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mcpayment.fd.data.entity.FdIndiaTransaction;
import com.mcpayment.fd.data.service.FdIndiaTransactionService;
import com.mcpayment.gateway.data.service.CurrencyService;
import com.mcpayment.gateway.data.service.SequenceService;
import com.mcpayment.gateway.data.service.TransactionService;
import com.mcpayment.module.constant.GatewayConstant;
import com.mcpayment.module.crypto.util.DataSecurity;
import com.mcpayment.module.host.fd.constants.Constants;
import com.mcpayment.module.host.fd.services.FdIndiaHostModuleService;
import com.mcpayment.module.model.transaction.TransactionMessage;
import com.mcpayment.module.model.transaction.TransactionState;
import com.mcpayment.module.model.transaction.TransactionType;
import com.mcpayment.paymentservice.error.PaymentServiceException;
import com.mcpayment.paymentservice.fd.FdIndiaConstant;
import com.mcpayment.paymentservice.fd.exception.FdIndiaModuleException;
import com.mcpayment.paymentservice.fd.service.FdIndiaPreAuthService;
import com.mcpayment.paymentservice.fd.service.FdIndiaSecureService;
import com.mcpayment.persistent.entity.Currency;
import com.mcpayment.persistent.entity.Transaction;
import com.mcpayment.util.AmountUtil;
import com.mcpayment.util.Const;
import com.mcpayment.util.NumberUtil;
import com.mcpayment.util.ResponseCode;
import com.mcpayment.util.StringUtils;

@Service("fdIndiaPreAuthService")
public class FdIndiaPreAuthServiceImpl implements FdIndiaPreAuthService {

    private static final Logger log = LoggerFactory.getLogger(FdIndiaPreAuthServiceImpl.class);

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private FdIndiaTransactionService fdIndiaTransactionService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private FdIndiaHostModuleService fdIndiaHostModuleService;

    @Autowired
    private FdIndiaSecureService fdIndiaSecureService;

    @Override
    public TransactionMessage init(TransactionMessage message) {
        log.info("PreAuth init method : {}", message);
        String cardNumber = message.getPan();
        if (StringUtils.isBlank(cardNumber)) {
            throw new FdIndiaModuleException("Invalid CardNumber.");
        }
        String cardExpiryDate = message.getCardExpiryDate();
        if (StringUtils.isBlank(cardExpiryDate)) {
            throw new FdIndiaModuleException("Invalid CardExpiryDate.");
        }
        String cvc = message.getCvc();
        if (StringUtils.isBlank(cvc)) {
            throw new FdIndiaModuleException("Invalid CVC.");
        }
        String totalAmount = message.getTotalAmount();
        if (StringUtils.isBlank(totalAmount)) {
            throw new FdIndiaModuleException("Invalid TotalAmount.");
        }
        String currency = message.getCurrency();
        if (StringUtils.isBlank(currency)) {
            throw new FdIndiaModuleException("Invalid Currency.");
        }

        return message;
    }

    @Override
    public TransactionMessage prepare(TransactionMessage message) {
        log.info("PreAuth prepare method : {}", message);
        Long currentStan = sequenceService.getNextStanSp(message.getAcceptorTerminalId());
        message.setStan(StringUtils.leftPad(String.valueOf(currentStan), 6, '0'));

        Currency currency = currencyService.getCurrencyByName(message.getCurrency());
        if (currency == null) {
            throw new FdIndiaModuleException("Not Find Currency." + currency);
        }
        message.setCurrencyCode(currency.getCode());

        fdIndiaSecureService.validateAndSet3DSecureInfo(message);

        return message;
    }

    @Override
    public TransactionMessage save(TransactionMessage message) {
        log.info("PreAuth save method : {}", message);
        Transaction transaction = new Transaction();
        transaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        transaction.setState(TransactionState.REQUESTED);
        message.setTransactionState(StringUtils.getString(transaction.getState().getId()));
        transaction.setSaleAmount(NumberUtil.getLong(message.getSalesAmount()));
        transaction.setServiceAmount(NumberUtil.getLong(message.getServiceAmount()));
        transaction.setGstAmount(NumberUtil.getLong(message.getGstAmount()));
        transaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        transaction.setServiceRate(NumberUtil.getLong(message.getServiceRate()));
        transaction.setGstRate(NumberUtil.getLong(message.getGstRate()));
        transaction.setStan(message.getStan());
        transaction.setGatewayRequestDate(new Date());
        transaction.setClientRequestDate(message.getClientRequestDate());
        transaction.setNewRouteId(message.getServerData().getRouteOid());
        transaction.setRemarks(message.getReferenceText());
        transaction.setAcceptorId(message.getAcceptorId());
        transaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
        transaction.setAcceptorIPPTerminalId(message.getServerData().getRoute().getAcceptorIPPTerminalId());
        transaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        transaction.setPan(message.getPan());
        transaction.setIccData(message.getIccData());
        transaction.setCardExpiryDate(message.getCardExpiryDate());
        transaction.setCardEntryMode(message.getCardEntryMode());
        transaction.setApplnPanSeq(message.getApplnPanSeq());
        transaction.setInstallment(message.getInstallment());
        transaction.setTruncatedPan(message.getTruncatedPan());
        transaction.setBrandName(message.getBrandName());
        transaction.setCardHolderName(message.getCardHolderName());
        transaction.setCurrency(message.getCurrency());
        transaction.setPosConditionCode(NumberUtil.getValidateInteger(message.getPosConditionCode()));
        transaction.setReferenceNo(message.getReferenceText());
        transaction.setReferenceNo(message.getParticularDataAsString(GatewayConstant.REFERENCE));
        transaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        transaction.setMcpMarkupRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        transaction.setMerchantId(message.getServerData().getMerchantOid());
        transaction.setMcpBank(message.getServerData().getRoute().getMcpBank());
        transaction.setMcpAccount(message.getServerData().getRoute().getMcpAccount());
        Long txnId = transactionService.insertTransaction(transaction);
        message.setTransactionId(String.valueOf(txnId));

        // This process should asynchronized task
        FdIndiaTransaction fdIndiaTransaction = new FdIndiaTransaction();
        fdIndiaTransaction.setId(txnId);
        fdIndiaTransaction.setType(TransactionType.getTransactionType(message.getTransactionType().getId()));
        fdIndiaTransaction
                .setState(TransactionState.getTransactionState(NumberUtil.getInteger(message.getTransactionState())));
        fdIndiaTransaction.setMchId(message.getAcceptorId());
        fdIndiaTransaction.setMcpTerminalId(message.getServerData().getMcpTerminalId());
        fdIndiaTransaction.setTransactionId(message.getTransactionId());
        fdIndiaTransaction.setTotalAmount(NumberUtil.getLong(message.getTotalAmount()));
        fdIndiaTransaction.setItemsDetails(message.getReferenceText());
        fdIndiaTransaction.setCurrency(message.getCurrency());
        fdIndiaTransaction.setTimeStart(new Date());
        fdIndiaTransaction.setBrandName(message.getBrandName());
        fdIndiaTransaction.setCvc(message.getCvc());
        fdIndiaTransaction.setCardHolderName(message.getCardHolderName());
        fdIndiaTransaction.setEncryptedXId(message.getParticularDataAsString(Constants.SECURE_XID));
        if (!StringUtils.isBlankOrNull(message.getParticularDataAsString(Constants.SECURE_ECI))) {
            fdIndiaTransaction.setEci(Integer.parseInt(message.getParticularDataAsString(Constants.SECURE_ECI)));
        }
        fdIndiaTransaction.setEncryptedCav(message.getParticularDataAsString(Constants.SECURE_CAV));
        fdIndiaTransaction.setBaseRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.BASE_RATE))));
        fdIndiaTransaction.setMcpMarkUpRate(
                NumberUtil.getLong(StringUtils.getString(message.getParticularData(FdIndiaConstant.MCP_MARKUP_RATE))));
        fdIndiaTransactionService.insertTransaction(fdIndiaTransaction);

        return message;
    }

    @Override
    public TransactionMessage send(TransactionMessage message) {
        // Send message to HM to process request to acquirer host
        try {
            log.info("PreAuth send method : {}", message);

            Currency currencyDest = currencyService.getCurrencyByName(message.getCurrency());
            if (currencyDest == null) {
                throw new FdIndiaModuleException("Not Find Currency." + message.getCurrency());
            }
            String amountSettled = AmountUtil.convertFromCents(message.getTotalAmount(), currencyDest.getExponent());
            BigDecimal bigDecimalAmount = new BigDecimal(amountSettled);
            BigDecimal bigDecimalAmountScale = bigDecimalAmount.setScale(currencyDest.getExponent(),
                    BigDecimal.ROUND_HALF_UP);
            message.setTotalAmount(bigDecimalAmountScale.toString());

            if (!StringUtils.isBlankOrNull(message.getPan())) {
                String decPan = DataSecurity.decAES256(message.getPan(), DataSecurity.md5Hash32(Const.HASH_KEY_PAN));
                message.setPan(decPan);
            }
            if (!StringUtils.isBlankOrNull(message.getCardExpiryDate())) {
                String decCardExpiry = DataSecurity.decAES256(message.getCardExpiryDate(),
                        DataSecurity.md5Hash32(Const.HASH_KEY_EXPIRY));
                message.setCardExpiryDate(decCardExpiry);
            }
            if (!StringUtils.isBlankOrNull(message.getTrackII())) {
                String trackII = DataSecurity.decAES256(message.getTrackII(),
                        DataSecurity.md5Hash32(Const.HASH_KEY_TRACK_II));
                message.setTrackII(trackII);
            }
            if (!StringUtils.isBlankOrNull(message.getTrackI())) {
                String trackI = DataSecurity.decAES256(message.getTrackI(),
                        DataSecurity.md5Hash32(Const.HASH_KEY_TRACK_I));
                message.setTrackI(trackI);
            }
            message = fdIndiaHostModuleService.preAuth(message);
        } catch (Exception e) {
            throw new PaymentServiceException(e);
        }
        return message;
    }

    @Override
    public TransactionMessage update(TransactionMessage message) {
        log.info("PreAuth update method : {}", message);
        try {
            // Update DB once response got from HM
            Long txnId = Long.parseLong(message.getTransactionId());
            Transaction transaction = transactionService.getTransactionById(txnId);
            log.info("PreAuth update method(transaction) : {}", transaction);
            if (transaction != null) {
                message.setTotalAmount(StringUtils.getString(transaction.getTotalAmount()));
                transaction.setHostResponseDate(message.getHostResponseDate());
                transaction.setGatewayResponseDate(message.getGatewayResponseDate());
                transaction.setAuthorizationDate(new Date());
                transaction.setAuthorizationCode(message.getAuthCode());

                // Set specific transaction data and insert to
                // FDINDIA_TRANSACTION, This process should asynchronized task
                FdIndiaTransaction fdIndiaTransaction = fdIndiaTransactionService.getTransactionById(txnId);
                log.info("PreAuth update method(fdIndiaTransaction) : {}", fdIndiaTransaction);
                if (ResponseCode.SUCCESS.equals(message.getHostResponseCode())) {
                    message.setHostResponseCode(ResponseCode.SUCCESS);
                    message.setGatewayResponseCode(ResponseCode.SUCCESS);

                    transaction.setState(TransactionState.OK);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setRrn(message.getRrn());
                    transaction.setReceiptNo(message.getReceiptNumber());

                    fdIndiaTransaction.setState(TransactionState.OK);
                    fdIndiaTransaction.setReceiptNo(message.getReceiptNumber());
                } else {
                    // message.setHostResponseCode(ResponseCode.REQUEST_DENIED);
                    message.setGatewayResponseCode(ResponseCode.REQUEST_DENIED);

                    transaction.setState(TransactionState.DENIED);
                    transaction.setHostResponseCode(message.getHostResponseCode());
                    transaction.setGatewayResponseCode(message.getGatewayResponseCode());
                    transaction.setErrorMessage(message.getHostResponseMessage() + " : " + message.getErrorMessage());

                    fdIndiaTransaction.setState(TransactionState.DENIED);
                    fdIndiaTransaction.setErrMsg(message.getErrorMessage());
                }
                transactionService.updateTransaction(transaction);

                fdIndiaTransaction.setCountryName(message.getParticularDataAsString(Constants.COUNTRY));
                fdIndiaTransaction.setPaymentType(message.getParticularDataAsString(Constants.PAYMENT_TYPE));
                fdIndiaTransaction.setResponseMsg(message.getHostResponseMessage());
                fdIndiaTransaction.setAcceptorTerminalId(message.getAcceptorTerminalId());
                fdIndiaTransaction.setHostTxnTime(message.getHostResponseDate());
                fdIndiaTransaction.setMcpTxnTime(new Date());
                fdIndiaTransaction.setTimeEnd(new Date());
                fdIndiaTransactionService.updateTransaction(fdIndiaTransaction);
            } else {
                throw new PaymentServiceException("Invalid Transaction.");
            }
        } catch (Exception e) {
            log.error("error {}", e);
        }
        return message;
    }
}

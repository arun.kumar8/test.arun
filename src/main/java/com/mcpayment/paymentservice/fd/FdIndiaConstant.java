package com.mcpayment.paymentservice.fd;

import java.math.BigDecimal;

public class FdIndiaConstant {

    public static final String HOST_NAME = "fd";
    public static final String TEMP_TXN_STATES = "states";
    public static final String TEMP_TRANSACTION_ID = "transactionId";
    public static final String FGKEY = "fgkey";
    public static final String BASE_RATE = "baseRate";
    public static final String MCP_MARKUP_RATE = "mcpMarkUpRate";

    public static final String SUCCESSFUL = "Y"; // N : FAILED

    public static final BigDecimal TIMES = new BigDecimal("100000000");

    public static final String INVALID_ECI_STR = "Transaction denied due to invalid ECI";
    public static final String INVALID_XID_STR = "Transaction denied due to invalid XID";
    public static final String INVALID_CAV_STR = "Transaction denied due to invalid CAV or CAVV";

}
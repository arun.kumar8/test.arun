package com.mcpayment.paymentservice.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import com.mcpayment.paymentservice.properties.DBProperties;

@Configuration
@EnableAutoConfiguration
public class Gw2DbConfig {

    @Autowired
    private DBProperties dbProp;

    @Bean
    @Primary
    public DataSource dataSource() throws PropertyVetoException {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(dbProp.getDbUrl());
        dataSource.setPassword(dbProp.getDbPassword());
        dataSource.setUsername(dbProp.getDbUserName());
        dataSource.setDriverClassName(dbProp.getDbDriverClass());
        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() throws PropertyVetoException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(new String[] {
            dbProp.getPackageScan()
        });
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", dbProp.getDbDialect());
        hibernateProperties.put("hibernate.show_sql", dbProp.isShowSql());
        hibernateProperties.put("hibernate.format_sql", dbProp.isFormatSql());
        hibernateProperties.put("hibernate.cache.use_second_level_cache", dbProp.isCache());
        hibernateProperties.put("hibernate.cache.provider_class", dbProp.getCacheProviderClass());
        hibernateProperties.put("hibernate.cache.region.factory_class", dbProp.getCacheFactoryClass());
        sessionFactoryBean.setHibernateProperties(hibernateProperties);
        return sessionFactoryBean;
    }
}
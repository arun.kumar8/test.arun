package com.mcpayment.paymentservice.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import com.mcpayment.paymentservice.properties.FdIndiaDBProperties;

@Configuration
@EnableAutoConfiguration
public class FdIndiaDbConfig {

    @Autowired
    private FdIndiaDBProperties fdIndiaDbProps;

    @Bean
    public DriverManagerDataSource fdIndiaDataSource() throws PropertyVetoException {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(fdIndiaDbProps.getDbUrl());
        dataSource.setPassword(fdIndiaDbProps.getDbPassword());
        dataSource.setUsername(fdIndiaDbProps.getDbUserName());
        dataSource.setDriverClassName(fdIndiaDbProps.getDbDriverClass());
        return dataSource;
    }

    @Bean("fdIndiaSessionFactory")
    public LocalSessionFactoryBean fdIndiaSessionFactory() throws PropertyVetoException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(fdIndiaDataSource());
        sessionFactoryBean.setPackagesToScan(new String[] {
            fdIndiaDbProps.getPackageScan()
        });
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", fdIndiaDbProps.getDbDialect());
        hibernateProperties.put("hibernate.show_sql", fdIndiaDbProps.isShowSql());
        hibernateProperties.put("hibernate.format_sql", fdIndiaDbProps.isFormatSql());
        hibernateProperties.put("hibernate.cache.use_second_level_cache", fdIndiaDbProps.isCache());
        hibernateProperties.put("hibernate.cache.provider_class", fdIndiaDbProps.getCacheProviderClass());
        hibernateProperties.put("hibernate.cache.region.factory_class", fdIndiaDbProps.getCacheFactoryClass());
        sessionFactoryBean.setHibernateProperties(hibernateProperties);
        return sessionFactoryBean;
    }
}
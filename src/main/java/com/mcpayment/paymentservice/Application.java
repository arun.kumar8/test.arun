package com.mcpayment.paymentservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.mcpayment.module.service.PaymentService;
import com.mcpayment.module.service.PaymentServices;

@SpringBootApplication
@ImportResource("ps-root.xml")
@EnableScheduling
public class Application {

    @Value("${gw2.address}")
    private String gw2Url;

    @Autowired
    private PaymentService fdIndiaPsService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public HttpInvokerProxyFactoryBean paymentServices() {
        final HttpInvokerProxyFactoryBean proxy = new HttpInvokerProxyFactoryBean();
        proxy.setServiceUrl(gw2Url + "/PaymentServicesRemote");
        proxy.setServiceInterface(PaymentServices.class);
        return proxy;
    }

    @Bean(name = "/fdIndiaHost")
    public HttpInvokerServiceExporter fdIndiaHost() {
        final HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setService(fdIndiaPsService);
        exporter.setServiceInterface(PaymentService.class);
        return exporter;
    }
}

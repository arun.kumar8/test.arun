package com.mcpayment.paymentservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fdIndia")
public class FdIndiaDBProperties {

    /* Hibernate Properties */
    private String packageScan;
    private boolean showSql;
    private boolean formatSql;
    private boolean cache;
    private String cacheProviderClass;
    private String cacheFactoryClass;

    /* DB Configuration */
    private String dbDriverClass;
    private String dbDialect;
    private String dbUrl;
    private String dbUserName;
    private String dbPassword;

    public String getPackageScan() {
        return packageScan;
    }

    public void setPackageScan(String packageScan) {
        this.packageScan = packageScan;
    }

    public boolean isShowSql() {
        return showSql;
    }

    public void setShowSql(boolean showSql) {
        this.showSql = showSql;
    }

    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public String getCacheProviderClass() {
        return cacheProviderClass;
    }

    public void setCacheProviderClass(String cacheProviderClass) {
        this.cacheProviderClass = cacheProviderClass;
    }

    public String getCacheFactoryClass() {
        return cacheFactoryClass;
    }

    public void setCacheFactoryClass(String cacheFactoryClass) {
        this.cacheFactoryClass = cacheFactoryClass;
    }

    public String getDbDriverClass() {
        return dbDriverClass;
    }

    public void setDbDriverClass(String dbDriverClass) {
        this.dbDriverClass = dbDriverClass;
    }

    public String getDbDialect() {
        return dbDialect;
    }

    public void setDbDialect(String dbDialect) {
        this.dbDialect = dbDialect;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public boolean isFormatSql() {
        return formatSql;
    }

    public void setFormatSql(boolean formatSql) {
        this.formatSql = formatSql;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

}

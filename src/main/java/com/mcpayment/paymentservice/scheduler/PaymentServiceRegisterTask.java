package com.mcpayment.paymentservice.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mcpayment.module.service.PaymentServices;

@Component
public class PaymentServiceRegisterTask {

    private static final Logger log = LoggerFactory.getLogger(PaymentServiceRegisterTask.class);

    @Autowired
    private PaymentServices paymentServices;

    @Value("${paymentService.name}")
    private String name;

    @Value("${paymentService.address}")
    private String address;

    private boolean errorControl = false;
    private boolean firstRegister = true;

    @Scheduled(fixedDelay = 5000)
    public void register() {
        try {
            if (paymentServices.registerPaymentService(name, address) || firstRegister) {
                firstRegister = false;
                errorControl = false;
                log.info("PaymentService: {} registered with success.", name);
            } else {
                Thread.sleep(30000);
            }
        } catch (Exception e) {
            if (!errorControl) {
                errorControl = true;
                log.warn("Can't register paymentService {}. {}", name, e);
            }
        }
    }

    public void setPaymentServices(PaymentServices paymentServices) {
        this.paymentServices = paymentServices;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }
}

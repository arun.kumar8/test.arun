// Gitlab connection in Jenkins > Configuration > Gitlab
properties([gitLabConnection('Gitlab Connection')])

gitlabBuilds(builds: ['unit test','build','dockerize','deploy','integration test']){

    // Run this pipeline in slave nodes tagged as 'linux-builder'
    node(label: 'linux-builder'){

        checkout scm

        stage ('unit test') {
            gitlabCommitStatus (name: "unit test") {
                // Use the Maven configured in Jenkins (via Global Tool)
                withMaven(maven: 'maven', options: [artifactsPublisher(disabled: true)]) {
                    sh "mvn clean verify -Pclover.all --update-snapshots -DskipTests"
                }
                // TODO: Add threshold for code coverage
                step([
                    $class: 'CloverPublisher',
                    cloverReportDir: 'target/site',
                    cloverReportFileName: 'clover.xml'
                  ])
            }
        }

        stage ('build') {
            gitlabCommitStatus (name: "build") {
                // Use the Maven configured in Jenkins (via Global Tool)
                withMaven(maven: 'maven', options: [artifactsPublisher(disabled: true)]) {
                    sh "mvn clean install --update-snapshots -DskipTests"
                }
                // Get project details we gonna use later
                pom = readMavenPom file: 'pom.xml'
            }
        }

        stage ('dockerize') {
            gitlabCommitStatus (name: "dockerize") {

                // Get MCP service type from linux-environment-config-deploy sheet
                // https://docs.google.com/spreadsheets/d/1ZYvxKO6uZH5SOP_1NywjxNRIhgOPMbvLF25f2ys7J5g
                ID='AKfycbztloPyIDWfdp4zy-Dk0N-SwMHSCOHDPlD8p4vkyOqS4taCU4hh'
                requestParams="requestType=getServiceType&appName=${pom.artifactId}"
                rawJson = httpRequest("https://script.google.com/a/mcpayment.com/macros/s/${ID}/exec?${requestParams}").content
                data = readJSON(text: rawJson)
                serviceType = data.serviceType

                // These environment variables were set in
                // Jenkins configuration > Global Properties
                def registryUrl = env.PIPELINE_REGISTRY_URL
                def registryCredentials = env.PIPELINE_REGISTRY_CREDENTIALS_ID
                def imageTag = pom.version

                // Build and push docker image to registry
                docker.withRegistry(registryUrl, registryCredentials) {
                    def customImage = docker.build("${serviceType}/${pom.artifactId}:${imageTag}")
                    customImage.push()
                }
            }
        }

        stage('deploy') {
            gitlabCommitStatus (name: "deploy") {
                // Call deployer pipeline
                build job: 'PIPELINE-DEVOPS/LINUX-APP-DEPLOYER',
                    parameters: [string(name: 'APP_NAME', value: pom.artifactId),
                                string(name: 'APP_VERSION', value: pom.version),
                                string(name: 'GIT_BRANCH', value: env.BRANCH_NAME)]
            }
        }

        stage('integration test') {
            gitlabCommitStatus (name: "integration test") {
                // Call integration test pipeline
                build job: 'PIPELINE-QA/TEST-SUITE',
                    parameters: [string(name: 'APP_NAME', value: pom.artifactId),
                                string(name: 'APP_VERSION', value: pom.version),
                                string(name: 'GIT_BRANCH', value: env.BRANCH_NAME)]
            }
        }

    }
}
